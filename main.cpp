#include <fstream>
#include <string>
#include <set>
#include <iostream>
#include <algorithm>
using namespace std;

ifstream infile;
ofstream outfile;

int main()
{
    infile.open("src.txt");

    // посчитаем всего строк
    string line;
    long totalLines = 0;
    long curreLines = 0;
    long lastPercent = 0;
    while (getline(infile, line)) {
        totalLines++;
    }

    cout << "Hello World!" << endl;

    // загружаем в результат
    infile.seekg(0, ios::beg);

    set<string> result;
    while (getline(infile, line))
    {
        // вдруг уже есть
        auto it = std::find_if(begin(result), end(result), [&line](const string& s) {
            return s == line;
        });

        if (it == std::end(result)) {
            result.insert(line);
        }

        // расчитываем новое значение процента
        curreLines++;
        int pc = curreLines*100/totalLines;
        if (pc != lastPercent) {
            cout << pc << "%" << endl;
        }
    }

    outfile.open("result.txt");
    for (auto &row : result) {
        outfile << row << endl;
    }


    return 0;
}

